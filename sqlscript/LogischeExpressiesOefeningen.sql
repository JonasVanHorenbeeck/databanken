1.selecteer boekenn van 2 Auteurs
use ModernWays;
Select * from Boeken
	where Voornaam = 'Gerard'or Voornaam = 'Jacob'
	
2. Selecteer boeken van alle auteurs behalve opgegeven auteur
use ModernWays;
Select * from Boeken
	where Voornaam != 'Gerard'
	
3. Voeg Hugo Claus, De verwondering, Antwerpen, Manteau, 1970; bij
use ModernWays;
insert into Boeken (
    Voornaam,
    Familienaam,
    Titel,
    Stad,
    Uitgeverij,
    VerschijningsJaar,
    InsertedBy)
Value(
    'Hugo',
    'Claus',
    'De Verwondering',
     'Antwerpen',
    'Manteau',
    '1970',
    'JonasVanHorenbeeck');
    
4. Voeg Hugo Raes, Jagen en gejaagd worden, Antwerpen, De Bezige Bij, 1954;toe
use ModernWays;
insert into Boeken (
    Voornaam,
    Familienaam,
    Titel,
    Stad,
    Uitgeverij,
    VerschijningsJaar,
    InsertedBy)
Value(
    'Hugo',
    'Raes',
    'Jagen en gejaagd worden',
     'Antwerpen',
    'De Bezige bij',
    '1954',
    'JonasVanHorenbeeck');

5.Voeg Jean-Paul Sarthe, Het zijn en het niets, 1943, Parijs, Gallimard; bij
use ModernWays;
insert into Boeken (
    Voornaam,
    Familienaam,
    Titel,
    Stad,
    Uitgeverij,
    VerschijningsJaar,
    InsertedBy)
Value(
    'Jean-Paul',
    'Sarthe',
    'Het Zijn En Het Niets',
     'Parijs',
    'Gallimard',
    '1943',
    'JonasVanHorenbeeck');
    
6. Selecteer alle boeken van de auteurs die de voornaam Hugo of Jean-Paul hebben;
use ModernWays;
select * from Boeken
	Where Voornaam = 'Hugo' or voornaam = 'Jean-Paul';
	
7. Selecteer alle boeken van de auteurs die de voornaam Hugo of Jean-Paul hebben en een boek in 1970 hebben geschreven;
use ModernWays;
select * from Boeken
	Where (Voornaam = 'Hugo' or voornaam = 'Jean-Paul')
	and Verschijningsjaar = 1970;
	
8. familienaam Sarthe verbeteren in Sartre zijn, familienaam
update Boeken
   set Familienaam = 'Sartre'
   where Familienaam = 'Sarthe';

9. Voeg de categorie 'Literatuur' toe voor de boeken van Hugo Claus en Hugo Raes.
update Boeken
   set Categorie = 'Literatuur'
   where (Familienaam = 'Claus' and Voornaam = 'Hugo') or
      (Familienaam = 'Raes' and Voornaam = 'Hugo');
      
10. 