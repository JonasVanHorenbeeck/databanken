
--4/6/2018
--David Hume toevoegen
use ModernWays;
insert into Personen(
    Voornaam,
    Familienaam,
    IdAanspreekTitel,
    Stad)
Values(
    'David',
    'Hume',
    1,
    'Edinburgh');
    
--3 boeken van hem toevoegen:
use ModernWays;
insert into Boeken(
    Titel,
    Stad,
    Uitgeverij,
    Verschijningsjaar,
    Herdruk,
    Commentaar,
    Categorie,
    IdAuteur,
    InsertedBy)
Values
	(
   		'Het menselijk inzicht',
   		'',
    	'Boom',
    	'2015',
    	'2',
        '',
        'Filosofie',
        (select Id from Personen where Familienaam = 'Hume'),
        'JonasVH'),
    (
        'Traktaat over de menselijke natuur',
   		'',
    	'Boom',
    	'2015',
    	'',
        '',
        'Filosofie',
        (select Id from Personen where Familienaam = 'Hume'),
        'JonasVH'),
    (
        'Treatise of Human Nature',
   		'',
    	'Penguin Books',
    	'2015',
    	'',
        '',
        'Roman',
        (select Id from Personen where Familienaam = 'Hume'),
        'JonasVH');