use ModernWays;
-- een pk kolom met naam Id toevoegen
alter table Postcodes add Id int auto_increment not null;
-- voeg een fk constraint toe aan Personen
alter table Postcodes add constraint pk_Postcodes_Id primary key(Id);
-- voeg een dummy postcode toe

insert into Postcodes (
   Code,
   Plaats,
   Provincie,
   Localite,
   Province
) 
values (
   '0000',
   'Niet van toepassing',
   'Niet van toepassing',
   'Non applicable',
   'Non applicable'
);

-- voeg een fk kolom toe als die nog niet bestaat
alter table Personen add IdPostcode int;
-- in de eerste 3 IdPostcode kolommen de waarde van de 8400 postcode plaatsen
update Personen set IdPostcode = (select Id from Postcodes where Code = '8400')
   where Id <= 3;
-- de volgende drie met 2000, 9000, 1000 enz
alter table Personen add 
   constraint fk_PersonenPostcodes_IdPostcode
   foreign key(IdPostcode)
   references Postcodes(Id);