-- ji
-- 28 februari 2018
-- Bestandsnaam: BoekenInsertAlfredDenker.sql
--
use ModernWays;
insert into Boeken (
    Voornaam,
    Familienaam,
    Titel,
    Stad,
    Uitgeverij,
    VerschijningsJaar,
    InsertedBy)
Value(
    'Alfred',
    'Denker',
    'Onderweg In Zijn En Tijd',
     '-',
    'Damon',
    '-',
    'JonasVanHorenbeeck');