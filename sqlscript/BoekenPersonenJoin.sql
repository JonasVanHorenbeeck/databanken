use ModernWays;
select Personen.Voornaam, Personen.Familienaam, Boeken.Titel
from Boeken
join Personen
on Boeken.IdAuteur = Personen.Id;

use ModernWays;
select 
Personen.Voornaam, Personen.Familienaam,
Boeken.Titel
from Boeken
inner join Personen
on Boeken.IdAuteur = Personen.Id
order by Personen.Familienaam desc;