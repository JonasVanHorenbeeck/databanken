USE ModernWays;
CREATE TABLE Personen (
    Id int NOT NULL AUTO_INCREMENT,
    Voornaam nvarchar(255) NOT NULL,
    Familienaam nvarchar(255),
    Leeftijd int,
    CONSTRAINT pk_Personen_Id PRIMARY KEY (Id)
);

use ModernWays;
insert into Personen (
    Voornaam,
	Familienaam,
    Leeftijd
	)
Value(
    'Sarah',
	'Coppens',
	30),
	('Jan',
    'Desomer',
    30);
    
    Zonder increment
    use ModernWays;
create table Personen2(
    Voornaam nvarchar(255)
    Familienaam nvarchar(255)
    Code char(4),
    CONSTRAINT pk_Personen2_Code primary key (Code)
    )