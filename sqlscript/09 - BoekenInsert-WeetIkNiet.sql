-- ji
-- 28 februari 2018
-- Bestandsnaam: BoekenInsert-WeetIkNiet.sql
--
use ModernWays;
insert into Boeken (
    Voornaam,
    Familienaam,
    Titel,
    Stad,
    Uitgeverij,
    VerschijningsJaar,
    InsertedBy)
Value(
    'Jonas',
    'VH',
    'WeetIkNiet',
     '-',
    'Damon',
    '-',
    'JonasVanHorenbeeck');