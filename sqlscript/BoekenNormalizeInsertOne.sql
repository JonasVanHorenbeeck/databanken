-- JI
-- 04 juni 2018
--
use ModernWays;
-- we gaan eerst een boek van een vrouw toevoegen
-- bestandnaam: BoekenNormalizeInsertOne.sql
--
-- auteur toevoegen
-- de Id van Mevrouw is 1, dat is de Id kolom van de Aanspreektitel tabel
insert into Personen (
   Voornaam, 
   Familienaam,
   IdAanspreekTitel
)
values (
   'Hilary', 
   'Mantel',
   1
);

select * from Personen;

-- ji
-- 8 januari 2018
--
use ModernWays;
-- we gaan eerst een boek van een vrouw toevoegen
-- bestandnaam: BoekenNormalizeInsertHilaryMantelOne.sql
--
-- auteur toevoegen
-- de Id van Meneer is 2, dat is de Id kolom van de Aanspreektitel tabel
insert into Personen (
   Voornaam,
   Familienaam,
   IdAanspreekTitel,
   Stad
)
values (
   'Jean-Paul',
   'Sartre',
   2,
   'Parijs'
);

select*from Personen;

-- bestandnaam: BoekenNormalizeInsertSartreOne.sql
use ModernWays;
insert into Boeken (
   Titel,
   Stad,
   Verschijningsjaar,
   Commentaar,
   Categorie,
   IdAuteur,
   InsertedBy
)

values (
   'De Woorden',
   'Antwerpen',
   '1962',
   'Een zeer mooi boek.',
   'Roman',
   (select Id from Personen where
      Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'),
   'JI');

select * from Boeken;