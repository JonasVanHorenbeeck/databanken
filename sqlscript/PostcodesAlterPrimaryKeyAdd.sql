-- NB
-- 27 april 2018
-- Bestandsnaam: PostcodesAlterPrimaryKeyAdd.sql
-- eerst een kolom Id toevoegen en dan dan de constraint toevoegen
use ModernWays;
ALTER TABLE Postcodes ADD Id INT auto_increment, ADD CONSTRAINT pk_Postcodes_Id PRIMARY KEY (Id);

